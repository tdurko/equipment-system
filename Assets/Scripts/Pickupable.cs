using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickupable : MonoBehaviour, IInteractable
{
    public Item item;

    public void Interact()
    {
        AddItemToInventory();
    }

    private void AddItemToInventory()
    {
        if(Inventory.instance.AddItem(item))
        {
            Debug.Log($"{item} has been added to inventory");
            Destroy(gameObject);
        }
        
    }
}
