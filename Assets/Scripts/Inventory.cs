using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public Dictionary<Item, int> items = new Dictionary<Item, int>();
    #region Singleton
    public static Inventory instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance Exist");
            Destroy(this);
        }
    }
    #endregion

    [SerializeField] private int _inventoryCapacity = 6;
    public event EventHandler OnInventoryChange;

    public bool AddItem(Item item)
    {
        var handler = OnInventoryChange;
        if (items.ContainsKey(item))
        {
            items[item]++;

            handler?.Invoke(this, null);
            return true;
        }
        if (items.Count < _inventoryCapacity)
        {
            items.Add(item, 1);
            handler?.Invoke(this, null);
            return true;
        }
        Debug.Log("Full EQ");
        return false;
    }

    public int GetCapacity()
    {
        return _inventoryCapacity;
    }

    public void RemoveItem(Item item)
    {
        var handler = OnInventoryChange;
        if (items[item] > 1)
        {
            items[item]--;
            handler?.Invoke(this, null);
            return;
        }
        items.Remove(item);
        handler?.Invoke(this, null);
    }
}
