using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Potion", menuName = "Inventory/Potion")]
public class Potion : Item
{
    public int amount = 15;
    public override void UseItem()
    {
        Debug.Log($"Wypito {base.itemName} dodano {amount} HP");
    }
}
