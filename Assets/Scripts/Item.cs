using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Item : ScriptableObject
{
    public string itemName;
    public string descripton;
    public Sprite icon;
    public virtual void UseItem()
    {
        Debug.Log($"Uzyto przedmiotu: {itemName}.");
    }
}
