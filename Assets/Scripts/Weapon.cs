using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Weapon", menuName = "Inventory/Weapon")]
public class Weapon : Item
{
    public int damage = 10;
    public override void UseItem()
    {
        Debug.Log($"Ubrano {base.itemName}. Bron zadaje {damage} obrazen");
    }
}
