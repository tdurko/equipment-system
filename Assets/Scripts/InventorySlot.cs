using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    Item _item;
    int _amount = 0;

    public Image icon;
    public Button useButton;
    public Button dropButton;
    public GameObject amountText;

    public void ChangeButtonsVisibility(bool value)
    {
        useButton.interactable = value;
        dropButton.interactable = value;
    }

    public void DropItem()
    {
        Inventory.instance.RemoveItem(_item);
    }

    public void UseItem()
    {
        _item.UseItem();
        Inventory.instance.RemoveItem(_item);
    }

    public void ActivateSlot()
    {
        if (_item != null)
        {
            InventoryUI.instance.SelectSlot(this);
            ChangeButtonsVisibility(true);
        }
    }

    public void DeactivateSlot()
    {
        ChangeButtonsVisibility(false);
    }

    public void AddItem(Item item,int amount)
    {
        _item = item;
        icon.sprite = _item.icon;
        icon.enabled = true;
        _amount = amount;
        ChangeAmountNumbers();
        ChangeAmountTextVisibilty(true);
    }
    public void ClearSlot()
    {
        _item = null;
        icon.sprite = null;
        icon.enabled = false;
        _amount = 0;
        ChangeAmountTextVisibilty(false);
    }

    void ChangeDescription()
    {
        DescriptionUI.instance.ChangeDescription(_item.itemName, _item.descripton);
    }

    void ChangeAmountNumbers()
    {
        amountText.GetComponent<Text>().text = _amount.ToString();
    }

    void ChangeAmountTextVisibilty(bool _value)
    {
        amountText.SetActive(_value);
    }
    public void OnPointerEnter()
    {
        if(_item != null)
        {
            DescriptionUI.instance.ToggleUI();
            ChangeDescription();
        }
    }
    public void OnPointerExit()
    {
        if (_item != null)
        {
            DescriptionUI.instance.ToggleUI();
        }
    }
}
