using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Clothes", menuName = "Inventory/Clothes")]
public class Clothes : Item
{
    public override void UseItem()
    {
        Debug.Log($"Ubrano {base.itemName}.");
    }
}
