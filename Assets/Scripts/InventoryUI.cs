using System;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    InventorySlot _selectedSlot;
    InventorySlot[] _slots;

    public GameObject inventoryUI;
    public Transform slotsParent;

    public bool freeSlots = true;
    #region Singleton
    public static InventoryUI instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance Exist");
            Destroy(this);
        }
    }
    #endregion

    Inventory _inventory; 
    private void Start()
    {
        _inventory = Inventory.instance;
        _slots = slotsParent.GetComponentsInChildren<InventorySlot>();
        _inventory.OnInventoryChange += Inventory_OnInventoryChange;
    }

    private void Inventory_OnInventoryChange(object sender, EventArgs e)
    {
        UpdateUI();
    }

    void UpdateUI()
    {

        int itemCounter = 0;
        foreach(var key in _inventory.items.Keys)
        {
            _slots[itemCounter].AddItem(key, _inventory.items[key]);
            _slots[itemCounter].DeactivateSlot();
            itemCounter++;
        }
        for(int i= itemCounter;i < _inventory.GetCapacity(); i++)
        {
            _slots[i].DeactivateSlot();
            _slots[i].ClearSlot();
        }

        //for (int i = 0; i < slots.Length; i++)
        //{
        //    if (i < inventory.items.Count)
        //    {
        //        slots[i].AddItem()
        //    }
        //    else
        //    {
        //        slots[i].ClearSlot();
        //    }
        //}
    }

    public void SelectSlot(InventorySlot InventorySlot)
    {
        if(_selectedSlot != null)
        {
            _selectedSlot.DeactivateSlot();
        }
        _selectedSlot = InventorySlot;
    }
    
    public void ToggleUI()
    {
        inventoryUI.SetActive(!inventoryUI.activeSelf);
    }


}
