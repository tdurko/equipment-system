using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Book", menuName = "Inventory/Book")]
public class Book : Item
{
    public override void UseItem()
    {
        Debug.Log($"Przeczytano {base.itemName}.");
    }
}
