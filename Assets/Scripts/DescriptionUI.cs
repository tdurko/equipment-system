using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DescriptionUI : MonoBehaviour
{
    #region Singleton
    public static DescriptionUI instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance Exist");
            Destroy(this);
        }
    }
    #endregion

    public GameObject descriptionUI;
    public Text nameText;
    public Text descriptionText;
    public void ChangeDescription(string itemName, string itemDescription)
    {
        nameText.text = itemName;
        descriptionText.text = itemDescription;
    }

    public void ToggleUI()
    {
        descriptionUI.SetActive(!descriptionUI.activeSelf);
    }
}