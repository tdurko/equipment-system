using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public Camera cam;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            IInteractable interactable;
            if (Physics.Raycast(ray, out hit))
            {
                interactable = hit.collider.GetComponentInParent<IInteractable>();
                if (interactable != null)
                {
                    interactable.Interact();
                }
            }
        }

        if(Input.GetKeyDown(KeyCode.Tab))
        {
            InventoryUI.instance.ToggleUI();
        }
    }
}
